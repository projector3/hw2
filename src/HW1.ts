import { argv } from "process";

// node HW1.js 4 0 0 0 1 1 0 1 1

interface Point {
	x: number;
	y: number;
}

export function main() {

	const points: Point[] = [];
	const length = +argv[2];
	for (let i = 3; i < 3 + length * 2; i += 2)
		points.push({ x: +argv[i], y: +argv[i + 1] });

	let maxPerimeter = 0;
	for (let i = 0; i < points.length; i++) {
		for (let j = i + 1; j < points.length; j++) {
			for (let k = j + 1; k < points.length; k++) {
				const perimeter = getPerimeter(points[i], points[j], points[k]);
				if (perimeter > maxPerimeter)
					maxPerimeter = perimeter;
			}
		}
	}

	console.log(maxPerimeter);
}

main();

function getPerimeter(point1: Point, point2: Point, point3: Point): number {
	const side1 = getDistance(point1, point2);
	const side2 = getDistance(point2, point3);
	const side3 = getDistance(point3, point1);

	return +(side1 + side2 + side3).toFixed(16);
}

function getDistance(point1: Point, point2: Point): number {
	return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
}