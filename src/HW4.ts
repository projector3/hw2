export function sinx(N: number, terms: number, x: number[], result: number[]) {
	const cache: { [i: number]: number } = {};
	for (let i = 0; i < N; i++) {
		let value = x[i];
		const idxValue = x[i];
		if (cache[idxValue]) {
			value = cache[idxValue];
		} else {
			let numer = idxValue * idxValue * idxValue;
			let denom = 6; // 3!
			let sign = -1;
			for (let j = 1; j <= terms; j++) {
				value += sign * numer / denom;
				numer *= idxValue * idxValue;
				denom *= (2 * j + 2) * (2 * j + 3);
				sign *= -1;
			}
			cache[x[i]] = value;
		}
		result[i] = value;
	}
}