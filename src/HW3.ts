const N: number = 4096;
const image: number[][] = new Array(N).fill(128).map(() => new Array(N).fill(128));

export function isDark(): boolean {
	let count = 0;
	for (let i = 0; i < N; ++i) {
		for (let j = 0; j < N; ++j)
			count += ((image[i][j] & 0x80) !== 0) ? 1 : 0;
	}

	return count < N * N / 2;
}