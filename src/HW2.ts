import { argv } from "process";

// node HW2.js 10 25 15 40

const X = +argv[2];
const Y = +argv[3];
const Z = +argv[4];
const W = +argv[5];

export function main() {

	if (X < 0 && X > 100)
		return;
	if (Y < 0 && Y > 100)
		return;
	if (Z < 0 && Z > 100)
		return;
	if (W < 0 && W > 1000)
		return;

	console.log(calculate(X, Y, Z, W));

	function calculate(X: number, Y: number, Z: number, W: number): number {
		let count = 0;

		for (let x = 0; x <= W / X; x++) {
			const remain1 = W - x * X;
			for (let y = 0; y <= remain1 / Y; y++) {
				const remain2 = remain1 - y * Y;
				if (remain2 % Z === 0) {
					count++;
				}
			}
		}

		return count;
	}
}

main();